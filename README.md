=================================**Spring Data JPA**====================================

This project will help you to understand the various options available to perform CRUD opertaions with Spring Data JPA.
It's a Spring Boot project where in-memory H2 database has been used.

You can learn the below things -
1. Query Methods
2. Parmeterized Query Methods
3. Async Query Methods
4. Position Based Query Method
5. Named Query Params
6. Custom Sort Method
7. Update Query Method
8. Delete Query Method
9. Named Query Method
10. Pagination
11. Sorting
12. Sorting with Pagination