package com.spring.data.jpa.datajpa.exception;

public class BusinessException extends Exception {
    public BusinessException(String message, Exception ex) {
        super(message, ex);
    }
}
