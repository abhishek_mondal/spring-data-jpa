package com.spring.data.jpa.datajpa.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Table(name = "Customer")
@NamedQuery(name = "Customer.findByFirstName" , query = "SELECT c from Customer c where c.firstName = ?1")
@NamedStoredProcedureQuery(name = "fetchAllCustomers" , procedureName = "FETCH_CUSTOMERS")
public class Customer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;

}
