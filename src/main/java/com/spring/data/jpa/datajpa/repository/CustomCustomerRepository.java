package com.spring.data.jpa.datajpa.repository;

import com.spring.data.jpa.datajpa.entity.Customer;

import java.util.List;

public interface CustomCustomerRepository {
    List<Customer> getAllCustomers();
}
