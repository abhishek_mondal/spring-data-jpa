package com.spring.data.jpa.datajpa.repository;

import com.spring.data.jpa.datajpa.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Future;

@Repository
@Transactional
public interface CustomerRepository extends CrudRepository<Customer, Long>, CustomCustomerRepository, PagingAndSortingRepository<Customer, Long>, JpaRepository<Customer, Long> {

    //Custom Query Methods
    List<Customer> findByLastName(String lastName);

    //Custom Query Methods
    int countByLastName(String lastName);

    //Custom Query Methods
    List<Customer> findDistinctByFirstName(String firstName);

    //Custom Query Methods
    List<Customer> findFirst3ByFirstNameOrderByFirstNameAsc(String firstName);

    //Custom Query Methods
    List<Customer> findByFirstNameAndLastName(String firstName, String lastName);

    //Query Method
    @Query("SELECT c from Customer c where c.id > :id")
    List<Customer> findByGreaterId(@Param("id") Long id);

    //Async Query Method
    @Async
    @Query("SELECT c from Customer c where c.id = :id")
    Future<Customer> findByIdAsyncTest(@Param("id") Long id);

    //Position Based Query Param QueryMethod
    @Query(value = "SELECT c from Customer c where firstName = ?1 AND lastName = ?2")
    List<Customer> testOfPositionParams(String firstName, String lastName);

    //Named Query Params
    @Query(value = "SELECT c from Customer c where c.firstName = :firstName AND c.lastName = :lastName")
    List<Customer> tesOfNamedParams(@Param("firstName") String firstName, @Param("lastName") String lastName);

    // sort customer in ascending order
    @Query("SELECT c FROM Customer c WHERE c.lastName = ?1 ORDER BY c.id DESC")
    List<Customer> findByLastNameOrderByIdAsc(String lastName);

    //Update Query Method
    @Modifying
    @Query("UPDATE Customer c SET c.lastName = ?1 WHERE c.id = ?2")
    int updateLastNameById(String lastName, Long id);

    //Delete Query Method
    @Modifying
    @Query("DELETE FROM Customer c WHERE c.id = ?1")
    void deleteById(Long id);

    //Named Query Method
    List<Customer> findByFirstName(String firstName);

    //Pagination 1 : Use Page<Customer> or Slice<Customer>
    Page<Customer> findAll(Pageable pageable);

    //Pagination 2
    List<Customer> findAllByLastName(String lastName, Pageable pageable);

    //Stored Procedures Call
    @Procedure(name = "fetchAllCustomers")
    List<Object[]> fetchAllCustomers();

    //Stored Procedures Call
    @Query(nativeQuery = true, value = "call FETCH_CUSTOMERS")
    List<Customer> getAllCustomers();

}
