package com.spring.data.jpa.datajpa.implementation;

import com.spring.data.jpa.datajpa.entity.Customer;
import com.spring.data.jpa.datajpa.repository.CustomCustomerRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

public class CutomCustomerRepoImlp implements CustomCustomerRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Customer> getAllCustomers() {
        StoredProcedureQuery findProcedure = entityManager.createNamedStoredProcedureQuery("fetchAllCustomers");
        return findProcedure.getResultList();
    }
}
